﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PrimitiveSnake_GameController : MonoBehaviour {
    ////////////Refs////////////
    public Text       scoreText;
    public GameObject loseText;
    public Text       RestartButton;
    public Button[]   buttonToDisable;
    public Object[] audioClips;
    ////////////Variables////////////
    public bool       firstLauch;
    public int        score;
    private bool      gamestarted;
    ////////////Prefabs////////////
    public GameObject cookiePrefab;
    public GameObject snakePrefab;    

    void Start()
    {
        firstLauch  = true; //Puts "Start" on game start button
        score       = 0;    //Reset of player score
        gamestarted = false;//Prevents game from creating cookies
    }
	

    void Update()
    {
        if (firstLauch == true) RestartButton.text = "Start";
        if (gamestarted == true) CheckOnCookies();
        Score();
    }


    //Menu
    public void Restart()      //Stops current game or creates new one
    {
        if (RestartButton.text == "Stop") LoseGame();
        else
        {
            Clear();
            Setup();
            gamestarted = true;
        } 
        firstLauch = false;
    }
    public void Quit()         //Goes off when player pushes exit button
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
    }
    void Score()               //Score Update
    {
        scoreText.text = "Score: " + score;
    }
    public void IncreaseScore()//Score increase with multiplier
    {
        score = score + GetComponent<PrimitiveSnake_GameConstants>().scoreMultiplier;
        GetComponent<AudioSource>().clip = (AudioClip)audioClips[0];
        GetComponent<AudioSource>().Play();
    }

    
    //Logic
    void CheckOnCookies()      //Creates cookies if necessary
    {
        GameObject[] allCookies = GameObject.FindGameObjectsWithTag("Coin"); //Getting all the cookies! *_*
        int maxCookies = GetComponent<PrimitiveSnake_GameConstants>().coins; //Getting info about how many cookies can be on map at once

        if(maxCookies > allCookies.Length)
        {
            SpawnCookie();
        }
    }
    void SpawnCookie()
    {
        //Getting all objects on the map to make sure that new cookie won't hit them
        GameObject[] Cookies = GameObject.FindGameObjectsWithTag("Coin"); 
        GameObject[] Snake = GameObject.FindGameObjectsWithTag("Snake");

        //Condition we're gonna check
        bool goodPosition = false;

        //Randomizing new cookies position (must be here, because in loop it "could" be skipped (#Visual_Studio_Errors_Logics)
        float randomX = (int)Random.Range(0, 47) - 23.5f;
        float randomZ = (int)Random.Range(0, 47) - 23.5f;

        while (goodPosition == false)
        {
            goodPosition = true;
            randomX = (int)Random.Range(0, 47) - 23.5f;
            randomZ = (int)Random.Range(0, 47) - 23.5f;

            //Checking collision
            for (int i = 0; i < Cookies.Length; i++)
            {
                if (randomX == Cookies[i].transform.position.x && randomZ == Cookies[i].transform.position.z) goodPosition = false;
            }
            for (int i = 0; i < Snake.Length; i++)
            {
                if (randomX == Snake[i].transform.position.x && randomZ == Snake[i].transform.position.z) goodPosition = false;
            }
            //if place is good object is created. If not - loop goes all over again.
        }

        Instantiate(cookiePrefab, new Vector3(randomX, 0, randomZ), transform.rotation);
    }

    //Setup
    void Clear()//Deleting all created objects from map, enabling level buttons
    {
        GameObject[] stuffToDelete;

        stuffToDelete = GameObject.FindGameObjectsWithTag("Coin");
        foreach (GameObject obj in stuffToDelete)
            Destroy(obj);
        stuffToDelete = GameObject.FindGameObjectsWithTag("Snake");
        foreach (GameObject obj in stuffToDelete)
            Destroy(obj);
        stuffToDelete = GameObject.FindGameObjectsWithTag("SnakeHead");
        foreach (GameObject obj in stuffToDelete)
            Destroy(obj);

        RestartButton.text = "Restart";
        foreach (Button x in buttonToDisable)
            x.interactable = true;
    }
    void Setup()//Reseting score, creating player, adjusting variables, changing UI
    {
        score = 0;
        GameObject tmp = (GameObject) Instantiate(snakePrefab, new Vector3(0.5f, 0.5f, 0.5f), transform.rotation);
        tmp.GetComponent<PrimitiveSnake_SnakeController>().sourceScript = GetComponent<PrimitiveSnake_GameConstants>();
        gamestarted = true;
        loseText.SetActive(false);
        RestartButton.text = "Stop";
        foreach (Button x in buttonToDisable)
            x.interactable = false;
    }
    public void LoseGame()
    {
        Clear();
        GetComponent<AudioSource>().clip = (AudioClip)audioClips[1];
        GetComponent<AudioSource>().Play();
        gamestarted = false;
        loseText.SetActive(true);
    }

}
