﻿using UnityEngine;
using System.Collections;

public class PrimitiveSnake_SegmentScript : MonoBehaviour {
    public GameObject nextSegment;

    public void Move(Vector3 position) //Script for moving whole snake tail
    {
        if (nextSegment != null) nextSegment.GetComponent<PrimitiveSnake_SegmentScript>().Move(transform.position);
        transform.position = position;
    }
}
