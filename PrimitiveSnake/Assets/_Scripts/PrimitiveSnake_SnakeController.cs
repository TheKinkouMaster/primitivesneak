﻿using UnityEngine;
using System.Collections;

public class PrimitiveSnake_SnakeController : MonoBehaviour {
    ////////////Variables//////////
    public char  lastMove;
    public char  plannedMove;
    public float timer;
    public float movementDelay;
    ////////////Prefabs//////////
    public GameObject snakePrefab;
    ////////////Refs//////////
    public PrimitiveSnake_GameConstants sourceScript;
    public GameObject                   nextSegment;

    

    void Start()
    {
        //Reseting movement
        lastMove = 'n';
        plannedMove = 'n';

        movementDelay = sourceScript.delay;
        timer = 0;
    }
    void Update()
    {
        //checking if delay has changed
        movementDelay = sourceScript.delay; 

        //getting keyboard orders
        if (getMovement() != 'n') plannedMove = getMovement();
        if ((plannedMove == 'w' && lastMove == 's')
            || (plannedMove == 's' && lastMove == 'w')
            || (plannedMove == 'a' && lastMove == 'd')
            || (plannedMove == 'd' && lastMove == 'a')
            || plannedMove == 'n')
            plannedMove = lastMove;

        //timer icreases
        timer = timer + Time.deltaTime;

        //movement
        if(timer >= movementDelay)
        {
            if (CheckCollision(plannedMove)) // crash with wall
                GameObject.FindGameObjectWithTag("GameController").GetComponent<PrimitiveSnake_GameController>().LoseGame();
            else //standard movement
            {
                MoveSnake(plannedMove);
                lastMove = plannedMove;
            }
            if (CheckCoinCollision() == true) EnlargeSnake();
            else MoveTail();
            timer -= movementDelay; //adjusting timer
        }
    }


    ///////////////////////////////////////////////////////////////////////////////////////
    private void MoveTail()
    {
        if (nextSegment != null)
        {
            Vector3 position = transform.position;

            if (lastMove == 'w') position += new Vector3(0, 0, -1);
            if (lastMove == 's') position += new Vector3(0, 0, 1);
            if (lastMove == 'a') position += new Vector3(1, 0, 0);
            if (lastMove == 'd') position += new Vector3(-1, 0, 0);
            nextSegment.GetComponent<PrimitiveSnake_SegmentScript>().Move(position);
        }
    }
    private void EnlargeSnake()//Just adding new segment. The rest of tail does not move
    {
        Vector3 position = transform.position;

        if (lastMove == 'w') position += new Vector3(0, 0, -1);
        if (lastMove == 's') position += new Vector3(0, 0, 1);
        if (lastMove == 'a') position += new Vector3(1, 0, 0);
        if (lastMove == 'd') position += new Vector3(-1, 0, 0);

        GameObject tmp = (GameObject)Instantiate(snakePrefab, position, transform.rotation);
        if (nextSegment == null) nextSegment = tmp;
        else
        {
            tmp.GetComponent<PrimitiveSnake_SegmentScript>().nextSegment = nextSegment;
            nextSegment = tmp;
        }
    }
    private void MoveSnake(char direction)
    {
        //preparing moving direction
        Vector3 movingDirection;
        movingDirection = new Vector3(0, 0, 0);

        if (direction == 'w') movingDirection += new Vector3(0, 0, 1);
        if (direction == 's') movingDirection += new Vector3(0, 0, -1);
        if (direction == 'a') movingDirection += new Vector3(-1, 0, 0);
        if (direction == 'd') movingDirection += new Vector3(1, 0, 0);

        //moving snake
        transform.Translate(movingDirection);
    }
    private bool CheckCollision (char direction)
    {
        //checking collision with edge in moving direction
        RaycastHit[] stuffHit;
        Vector3 movingDirection = new Vector3(0, 0, 0);

        //Creating moving vector
        if (direction == 'w') movingDirection += new Vector3(0, 0, 1);
        if (direction == 's') movingDirection += new Vector3(0, 0, -1);
        if (direction == 'a') movingDirection += new Vector3(-1, 0, 0);
        if (direction == 'd') movingDirection += new Vector3(1, 0, 0);

        //casting ray
        stuffHit = Physics.RaycastAll(transform.position, movingDirection, 1f);

        //checking hit
        for (int i = 0; i < stuffHit.Length; i++)
        {
            if (stuffHit[i].transform.tag == "Edge" || stuffHit[i].transform.tag == "Snake") return true;
        }

        return false;
    }
    private char getMovement()
    {
        char result = 'n';

        if (Input.GetKey(KeyCode.LeftArrow))  result = 'a';
        if (Input.GetKey(KeyCode.RightArrow)) result = 'd';
        if (Input.GetKey(KeyCode.DownArrow))  result = 's';
        if (Input.GetKey(KeyCode.UpArrow))    result = 'w';

        return result;
    }
    bool CheckCoinCollision()
    {
        GameObject[] Coins = GameObject.FindGameObjectsWithTag("Coin");
        foreach(GameObject coin in Coins)
            if(coin.transform.position.x == transform.position.x && coin.transform.position.z == transform.position.z)
            {
                Destroy(coin);
                GameObject.FindGameObjectWithTag("GameController").GetComponent<PrimitiveSnake_GameController>().IncreaseScore();
                return true;
            }
        return false;
    }
}
