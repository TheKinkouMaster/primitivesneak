﻿using UnityEngine;
using System.Collections;

public class PrimitiveSnake_GameConstants : MonoBehaviour {
    public float delay;            //Delay between snake "jumps" on map
    public int   coins;            //Max number of coins on map
    public int   scoreMultiplier;  //Multiplier changes with level

    void Start()
    {
        ChangeLevel("Hard");
    }

    public void ChangeLevel(string mode)
    {
        if (mode == "Easy")
        {
            delay = 0.4f;
            coins = 3;
            scoreMultiplier = 1;
        }
        if (mode == "Medium")
        {
            delay = 0.2f;
            coins = 2;
            scoreMultiplier = 2;
        }
        if (mode == "Hard")
        {
            delay = 0.05f;
            coins = 1;
            scoreMultiplier = 4;
        }
    }
}
